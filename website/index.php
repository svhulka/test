<?php

require_once('autoload.php');

$vehicles = [
    new \Model\Auto('bmw'),
    new \Model\Boat('boat'),
    new \Model\Helicopter('helicopter'),
    new \Model\Truck('kamaz'),
    new \Model\Motorcycle('harley-davidson')
];
foreach ($vehicles as $vehicle) {
    $line = '';
    switch ($vehicle->getName()) {
        case 'bmw':
            $line .= $vehicle->move() . '<br />';
            $line .= $vehicle->musicOn() . '<br />';
            break;
        case 'boat':
            $line .= $vehicle->swim() . '<br />';
            break;
        case 'helicopter':
            $line .= $vehicle->takeOff() . '<br />';
            $line .= $vehicle->fly() . '<br />';
            $line .= $vehicle->landing() . '<br />';
            break;
        case 'kamaz':
            $line .= $vehicle->move() . '<br />';
            $line .= $vehicle->stop() . '<br />';
            $line .= $vehicle->emptyLoads() . '<br />';
            break;
        case 'harley-davidson':
            $line .= $vehicle->move() . '<br />';
            $line .= $vehicle->stop() . '<br />';
            break;
        default:
            throw new \Exception('Undefined type of vehicle');
    }
    $line .= $vehicle->stop() . '<br />';
    $line .= $vehicle->refuel('gas') . '<br />';
    echo $line . '<hr />';
}