<?php

namespace Model;

/**
 * Class Truck
 * @package Model
 */
class Truck extends AbstractVehicle
{
    /**
     * @return string
     */
    public function move() : string
    {
        return in_array($this->getName(), self::MOVING_VEHICLES) ? ($this->getName() . ' moveing.') : '';
    }

    /**
     * @return string
     */
    public function stop() : string
    {
        return $this->getName() . ' stopped.';
    }

    /**
     * @return string
     */
    public function musicOn() : string
    {
        return $this->getName() . ' music switched on.';
    }

    /**
     * @return string
     */
    public function emptyLoads() : string
    {
        return $this->getName() . ' refuel.';
    }
}
