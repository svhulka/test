<?php

namespace Model;

/**
 * Class AbstractVehicle
 * @package Model
 */
abstract class AbstractVehicle
{
    /**
     * @const
     */
    const MOVING_VEHICLES = [
        'bmw',
        'kamaz',
        'harley-davidson'
    ];

    /**
     * @var string
     */
    private $name;

    /**
     * AbstractVehicle constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $oil
     *
     * @return string
     */
    public function refuel(string $oil) : string
    {
        return $this->name . ' refuel ' . $oil;
    }
}
