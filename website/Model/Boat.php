<?php

namespace Model;

/**
 * Class Boat
 * @package Model
 */
class Boat extends AbstractVehicle
{
    /**
     * @return string
     */
    public function swim() : string
    {
        return $this->getName() . ' swimming.';
    }

    /**
     * @return string
     */
    public function stop() : string
    {
        return $this->getName() . ' stopped.';
    }
}
