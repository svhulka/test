<?php

namespace Model;

/**
 * Class Motorcycle
 * @package Model
 */
class Motorcycle extends AbstractVehicle
{
    /**
     * @return string
     */
    public function move() : string
    {
        return in_array($this->getName(), self::MOVING_VEHICLES) ? ($this->getName() . ' moveing.') : '';
    }

    /**
     * @return string
     */
    public function stop() : string
    {
        return $this->getName() . ' stopped.';
    }

    /**
     * @return string
     */
    public function musicOn() : string
    {
        return $this->getName() . ' music switched on.';
    }
}
