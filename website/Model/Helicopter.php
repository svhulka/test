<?php

namespace Model;

/**
 * Class Helicopter
 * @package Model
 */
class Helicopter extends AbstractVehicle
{
    /**
     * @return string
     */
    public function stop() : string
    {
        return $this->getName() . ' stopped.';
    }

    /**
     * @return string
     */
    public function musicOn() : string
    {
        return $this->getName() . ' music switched on.';
    }

    /**
     * @return string
     */
    public function takeOff() : string
    {
        return $this->getName() . ' took off.';
    }

    /**
     * @return string
     */
    public function landing() : string
    {
        return $this->getName() . ' landing.';
    }

    /**
     * @return string
     */
    public function fly() : string
    {
        return $this->getName() . ' flying.';
    }
}
