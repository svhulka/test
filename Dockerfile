FROM registry.dev.pbp/base/php7:php71
COPY  . /usr/src/app
CMD ["php-fpm7.1", "-F"]